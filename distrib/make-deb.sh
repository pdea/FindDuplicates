#!/bin/bash
FDVER=$(git describe --tags --first-parent --abbrev=8)

# Fail on first error and show details
set -eET

failure() {
  local line=$1
  local msg=$2
  echo "Failed at $line: $msg"
}
trap 'failure ${LINENO} "$BASH_COMMAND"' ERR

# Set current directory to the one containing this script
cd "$(dirname "$0")"

# Create the output directory
mkdir -p download

echo -e "\e[36mCreating deb package...\e[0m"

FDDEBDIR=deb/findduplicates_$FDVER-1_amd64

rm -rf $FDDEBDIR
mkdir -p $FDDEBDIR
mkdir -p $FDDEBDIR/DEBIAN

# desktop file
DTFDIR=$FDDEBDIR/usr/share/applications
mkdir -p $DTFDIR
cp ../Sources/findduplicates.desktop $DTFDIR/
sed -i "s/FDVER/$FDVER/" $DTFDIR/findduplicates.desktop

# icon
ICDIR=$FDDEBDIR/usr/share/icons/hicolor/64x64/apps
mkdir -p $ICDIR
cp ../Sources/IconSources/fd_icone64.png $ICDIR/findduplicates.png

# program files
TARGETDIR=$FDDEBDIR/usr/share/findduplicates
mkdir -p $TARGETDIR
cp ../Sources/FindDuplicates.py $TARGETDIR
cp ../Sources/FindDuplicatesConfig.py $TARGETDIR
cp ../Sources/FindDuplicatesQtGui.py $TARGETDIR
cp ../Sources/Qt5GeneratedGui.py $TARGETDIR
cp ../Sources/Qt5GeneratedScrollDialogBox.py $TARGETDIR
cp ../README.md $TARGETDIR
cp "../README FR.md" $TARGETDIR
cp ../LICENSE $TARGETDIR

chmod 755 $TARGETDIR/FindDuplicatesQtGui.py

# debian files
SIZE=$(du -sk $FDDEBDIR | cut -f 1)

cat > "$FDDEBDIR/DEBIAN/control" <<EOL
Package: findduplicates
Version: $FDVER-1
Architecture: all
Maintainer: Florent Pichot <florent.pdea@gmail.com>
Section: utils
Priority: optional
Depends: python3, python3-pyqt5
Installed-Size: $SIZE
Homepage: https://gitlab.com/pdea/FindDuplicates/
Description: Find and delete duplicated files in one or more directories
EOL

mkdir -p $FDDEBDIR/usr/bin
ln -s ../share/findduplicates/FindDuplicatesQtGui.py  $FDDEBDIR/usr/bin/findduplicates

tput setaf 3
echo "Creating $FDDEBDIR.deb..."
fakeroot dpkg-deb --build $FDDEBDIR
# Move the .deb in the parent directory
mv -v $FDDEBDIR.deb ./download
tput sgr0

