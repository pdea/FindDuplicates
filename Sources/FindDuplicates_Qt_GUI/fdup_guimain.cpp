#include "fdup_guimain.h"
#include "ui_fdup_guimain.h"

FDup_GuiMain::FDup_GuiMain(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::FDup_GuiMain)
{
    ui->setupUi(this);
}

FDup_GuiMain::~FDup_GuiMain()
{
    delete ui;
}
