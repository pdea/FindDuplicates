#!/bin/bash

currentPath=`pwd`
destDir=TempDup
python3Path=python3

echo "Create duplicates for tests"
mkdir $destDir                        
cp -f File1.txt $destDir/File1Bis.txt 
cp -f File1.txt $destDir/File1Ter.txt 
cp -f File1.txt $destDir/File1Qua.txt 
cp -f File2.txt $destDir/File2Bis.txt 
cp -f File3.txt $destDir/File3Bis.txt 

echo "Launch FindDuplicate"
$python3Path ../FindDuplicatesQtGui.py -c TestConfig.xml
