import sys, os
from PyQt5 import QtCore, QtGui, QtWidgets
from cx_Freeze import setup, Executable

dirNamePath = os.path.dirname(os.path.abspath(__file__))
buildFolder = dirNamePath + "\\exe"

#Clean up old built files
filesToNotSupress = ["Builder.py", "FindDuplicates.py", "FindDuplicatesQtGUI.py", "Qt5GeneratedGui.py", "FindDuplicatesConfig.py", "LaunchBuilder.py", "ReadMe.txt"]


# List creation with the packets to add
build_exe_options = {"packages": ["PyQt5"], "includes": [], "excludes": ["Tkinter"], "build_exe": buildFolder}

# GUI applications require a different base on Windows (the default is for a console application).
base = None
if sys.platform == "win32":
    base = "Win32GUI"

setup(  name = "FindDuplicates",
        version = "2.0",
        description = "Application to find and help you manage duplicate files",
        options = {"build_exe": build_exe_options},
        executables = [Executable(dirNamePath + "\FindDuplicatesQtGUI.py", base=base)])
