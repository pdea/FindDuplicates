@echo off

@rem *****************************************************************************
@rem *** For Windows only ***
@rem *****************************************************************************
@rem Build FindDuplicate from python sources to windows exe
@rem   change icon of generated file
@rem   and create archive of all files in above directory

@rem Note: rcedit can be found here : https://github.com/electron/rcedit/releases
@rem   set RcEditPath below where batch file can find it : 
@rem       full path or exe name if in system path of in FindDuplicates directory

@rem set PATH_TO_7ZIP_EXE where 7zip is present
@rem *****************************************************************************

@rem be sure to launch correct version of python (python 3 needed)
@set Python3Path="c:\Program Files\Python39\python.exe"

@rem Path to rcedit version
@set RcEditPath=rcedit-x86.exe

@rem path to 7z exe. Change if necessary
@set PATH_TO_7ZIP_EXE="C:\Program Files\7-Zip\7z.exe"



@echo Delete all previously generated file
@del /Q exe\*.* >nul 2>&1

@echo Generate windows exe from python sources
@%Python3Path% Builder.py build

@echo Copy info files
@copy "..\README.md" exe\ReadMe.txt >nul 2>&1
@copy "..\README FR.md" exe\ReadMe.fr.txt >nul 2>&1

@echo Copy icon in exe directory
@copy FindDuplicates.ico exe >nul 2>&1

@echo Set this icon as generated exe icon
@%RcEditPath% exe\FindDuplicatesQtGui.exe --set-icon exe/FindDuplicates.ico >nul 2>&1

@echo Delete previous version of archive
@del /Q ..\FindDuplicates.exe.7z >nul 2>&1

@echo Generate archive file
@cd exe 
@rem zip exe and depedencies into a zip file
@%PATH_TO_7ZIP_EXE% A ..\..\FindDuplicates.exe.7z *
cd ..
